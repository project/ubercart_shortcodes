<?php
/**
 * Pricediff Shortcode Tag theme file
 *
 * @file
 * An example theme implementation that can be copied to your active theme.
 *
 * This file does nothing until you copy it into your active theme folder
 * and clear the theme registry cache.
 *
 * @author Dan Myers (designtodigital) <dan at designtodigital dot com>
 */
?>
<?php if ($price > 0): ?>
  <span class="<?php print $class; ?>"><?php print uc_price($price, array()); ?></span>
<?php else: ?>
  <span class="<?php print $class; ?>">N/A</span>
<?php endif; ?>