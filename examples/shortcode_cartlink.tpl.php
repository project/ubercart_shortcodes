<?php
/**
 * Cartlink Shortcode Tag theme file
 *
 * @file
 * An example theme implementation that can be copied to your active theme.
 *
 * This file does nothing until you copy it into your active theme folder
 * and clear the theme registry cache.
 *
 * @author Dan Myers (designtodigital) <dan at designtodigital dot com>
 */
?>
<?php if (!empty($text)): ?>
  <span class="<?php echo $class; ?>"><?php echo l(t($text), $path, array('html' => TRUE)); ?></span>
<?php else: ?>
  <?php echo url($path); ?>
<?php endif; ?>
