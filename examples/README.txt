The files provided in this folder are example template files.  To customize the output of any of the shortcodes, copy the corresponding file into your active theme folder, then clear your theme registry.

For example, to change how the sellprice shortcodes output html:
1. Copy shortcode_sellprice.tpl.php into your active theme folder.
2. Edit shortcode_sellprice.tpl.php with a plain text editor. 
3. In Drupal, go to Administer > Site Configuration > Performance.
4. Click the "Clear cached data" button at the bottom of the page.
5. Drupal is now using your customized theme file.
