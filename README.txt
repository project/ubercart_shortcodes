Quick Start Instructions:

1. Download and install the Shortcode module
2. Install this module
3. Go to Site Configuration > Input Formats
4. Click Configure next to the Full HTML Input Format
5. Check the Shortcodes option under Filters
6. Click Save configuration

You're now ready to use shortcodes in any text area that uses the Full HTML input format. Create a new Page node and change the input format to Full HTML. In the body of the page, add the text [cartlink sku="UC-123"][sellprice sku="UC-123" /][/cartlink] (Change UC-123 to the model number of a product in your store). When you view the page, you should see the price of that product. When clicked, it should add that product to your cart.

If you have any questions or comments, please post them to this project's issue queue.